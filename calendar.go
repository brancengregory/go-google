package google

import (
	"io/ioutil"
	"log"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	calendar "google.golang.org/api/calendar/v3"
)

var (
	configFile string
	service    *calendar.Service
)

type Event struct {
	*calendar.Event
}

type Events []Event

func Configure(s *calendar.Service) {
	service = s
}

//Init sets up the Google API connection
func Init(config string) *calendar.Service {
	configFile = config
	cred, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatalf("Unable to read JSON credentials config %v", err)
	}

	conf, err := google.JWTConfigFromJSON(cred, "https://www.googleapis.com/auth/calendar")
	if err != nil {
		log.Fatalf("Unable to obtain JWT conf %v", err)
	}

	client := conf.Client(oauth2.NoContext)

	service, err := calendar.New(client)
	if err != nil {
		log.Fatalf("Unable to create Google Calendar service from client %v", err)
	}

	return service
}

func GetEventsByMonth(t time.Time) (*calendar.Events, error) {
	t0 := time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, t.Location())
	t1 := time.Date(t.Year(), t.Month()+1, -1, 0, 0, 0, 0, t.Location())
	events, err := service.Events.List("info@gcdsa.org").ShowDeleted(false).
		SingleEvents(true).TimeMin(t0.Format(time.RFC3339)).TimeMax(t1.Format(time.RFC3339)).OrderBy("startTime").Do()
	if err != nil {
		log.Printf("Unable to retrieve next ten of the user's events: %v", err)
		return nil, err
	}

	return events, nil
}

//GetUpcomingEvents gets all events from the primary calendar
func GetUpcomingEvents(n int) (*calendar.Events, error) {
	t := time.Now().Format(time.RFC3339)
	events, err := service.Events.List("info@gcdsa.org").ShowDeleted(false).
		SingleEvents(true).TimeMin(t).MaxResults(int64(n)).OrderBy("startTime").Do()
	if err != nil {
		log.Printf("Unable to retrieve next ten of the user's events: %v", err)
		return nil, err
	}

	return events, nil
}

func GetEventByID(id string) (*calendar.Event, error) {
	event, err := service.Events.Get("info@gcdsa.org", id).Do()
	if err != nil {
		log.Print("couldnt get that event buxcko")
		return nil, err
	}

	return event, nil
}
